<?php

class Building{

	/*
	Access Modifiers
	1. 'public' keyword. it is fully open; properties and methods can be accessed from everywhere.
	This keyword can be used to control the visibility of properties and methods in a class.
	2. 'private' keyword. methods and properties can only be accessed within the class and disables inheritances
	3. 'protected' keyword. methods and properties are only accessible within the class and on its child class.
	*/

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// 'Encapsulation' indicates that data must not be directly accessible to users, but can be access only through public functions (setter and getter function)

	/*
	Getters and Setters
	These are used to retrieve and modify values of each property of the object.
	Each property of an object should have a set of getter and setter function

	getter (accessors) - this is used to retrieve/access the value of an instantiated object.

	setter (mutators) - this is used to change/modify the default value of a property of an instantiated object.
	*/
	
	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;

		// setter functions can also be modified to add data validations

		// if(gettype($name) === 'string') {
		// 	$this->name = $name;
		// }
	}

	public function getFloors() {
		return $this->floors;
	}

	private function setFloors($floors) {
		$this->floors = $floors;
	}

	public function getAddress() {
		return $this->address;
	}

	private function setAddress($address) {
		$this->address = $address;
	}
}

class Condominium extends Building{
}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");
