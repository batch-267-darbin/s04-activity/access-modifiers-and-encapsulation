<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04 Access Modifiers and Encapsulation</title>
	</head>
	<body>

		<h1>Access Modifiers</h1>

		<h2>Building Object</h2>
		<p><?php //echo $building->name; ?></p>
		<!-- You will receive a "Warning: Undefined property: Condominium::$name". -->
		<!-- This is because condominium no longer inherits the "name" property  due to its "private" modifier in the Building class.-->
		<!-- If the modifier is changed again to "protected", an error will be displayed "Uncaught Error: Cannot access protected property Condominium::$name" -->
		<!-- This is because we cannot directly access properties with "protected" access modifier, but compared with the previous warning displayed in the browser we are now able to inherit building properties. -->

		<h2>Condominium Object</h2>
		<p><?php //echo $condominium->name; ?></p>

		<h1>Encapsulation</h1>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

		<?php //$condominium->setName("Enzo Tower"); ?>
		<!-- <p>The name of the condominium is <?php echo $condominium->getName(); ?></p> -->


		<!-- S04 Activity -->

		<h2>Building</h2>
		<p>The name of the building is <?= $building->getName(); ?>.</p>
		<p>The <?= $building->getName(); ?> has <?= $building->getFloors() ?> floors.</p>
		<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>

		<?php $building->setName("Caswynn Complex"); ?>
		<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>

		<h1>Condominium</h2>
		<p>The name of the condominium is <?= $condominium->getName(); ?></p>
		<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
		<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>
		<?php $condominium->setName("Enzo Tower"); ?>
		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>
	</body>
</html>